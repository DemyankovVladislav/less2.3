#!/bin/sh
sleep 5
if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0,1
    done

    echo "PostgreSQL started"
fi

python manage.py migrate --no-input

python manage.py collectstatic --no-input --clear

#python manage.py runserver 0.0.0.0:8000 

exec gunicorn Blog.wsgi:application -b 0.0.0.0:8000 --reload

exec "$@"
